elvish_group = {
	graphical_cultures = {
		frankish_group_coa_gfx
		western_coa_gfx
		western_building_gfx
		indian_clothing_gfx
		mena_clothing_gfx
		western_unit_gfx
	}
	mercenary_names = {
	}
	
	moon_elvish = {
		
		color = { 135 245 245 }
		
		cadet_dynasty_names = {
			{ "dynnp_sil" }
			{ "dynnp_sil" }
		}

		dynasty_names = {
			{ "dynnp_sil" }
		}

		male_names = {
			Munarion Galinael Galindor Jahelor Urion Eborian Calasandal Calasandur Erelas Nesterin Filinar Erendil Thalanil Vindal Thalanor Vaceran 
			Darastarion Celador Celadorian Finorian Aldarian Aldarion Alarian Arfil Celadil Elethor Elethar Eledas Threthinor Pelodir Ultarion Carodir Calrodir 
			Aranthir Ivran Ivrandir Ivrandil Varilor Elanil Dorendor Varamael Varamel Varamelian Varamar Taelarios Taelar Taelarian Elrian Elriandor Varanar Camnar Camnarian 
			Camnaril Gelehorn Galindil Ardor Ardorian Artorian Thelrion Adrahel Denarion Calindal Serondor Serondar Serondal Gelmonias Thirendil Thirendir Erlanil Eranil Triandil 
			Evindal Oloris Darandil Alvarion Andrellion
		}
		female_names = {
			Galindel Istralania Narawen Erelassa Filinara Celadora Thaliandel Selussa Imariel Vehari Veharia Varila Varilia Panoril Liandiel Varinna Ariathra Ariathen Amarien 
			Alarawel Ladrindel Leslindel Lithiel Elissa Dasirna Meira Novia Ianren Ioria Narwen Istrala Varana Galinea Uria Eralia Nestara Vinda Vacera Erana Olora Andrellia
			Alvara Arda Ivrana Arantha Calindiel Gelmona Serona Ultariel Alaria Celada Elethiel Adral Variel Finoriel Calasiel
		}

		dynasty_of_location_prefix = "dynnp_sil"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			25 = moon_elvish
			5 = sun_elvish
		}
	}
	
	sun_elvish = {
	
		graphical_cultures = {
			arabic_group_coa_gfx
			mena_building_gfx
			indian_clothing_gfx
			mena_unit_gfx
			dde_abbasid_clothing_gfx
		}
		
		color = { 255 200 92 }
		
		cadet_dynasty_names = {
			{ "dynnp_szel" }
			{ "dynnp_szel" }
		}

		dynasty_names = {
			{ "dynnp_szel" }
		}

		male_names = {	#Sun Elves in EU4 currently use existing elven style and not just replacing s and z's - need to revisit and make a proper decision later

			# Munaraz Galinar Galindar Jahelar Jaher Urian Eborian Calazandal Calazandar Erelaz Nezterin Viliner Erendul Thalanul Vundal Thalunar Vaceran 
			# Darazarian Kelader Keladarian Vinarian Adarian Alarian Arvul Zeldal Elether Eledaz Treziner Kelidir Ultarian Caroder Calrudar 
			# Aranther Ivran Ivrander Ivrander Varilar Elaner Darendar Varamil Varamel Varamelaz Varamar Talaraz Talar Talrazan Elrian Elriandor Varanar Camnar Camnarian 
			# Camrel Gelehar Galindar Arder Ardoran Arturian Thelran Adrazel Derian Calindar Serandar Serandaz Serander Gelmoniaz Thirendaz Thirendar Erlanar Eranaz Triandar 
			# Evindar Uliriz Darandar Alvarian Andrellar Elisar Jaddar Jaerel

			Munarion Galinael Galindor Jahelor Urion Eborian Calasandal Calasandur Erelas Nesterin Filinar Erendil Thalanil Vindal Thalanor Vaceran 
			Darastarion Celador Celadorian Finorian Aldarian Aldarion Alarian Arfil Celadil Elethor Elethar Eledas Threthinor Pelodir Ultarion Carodir Calrodir 
			Aranthir Ivran Ivrandir Ivrandil Varilor Elanil Dorendor Varamael Varamel Varamelian Varamar Taelarios Taelar Taelarian Elrian Elriandor Varanar Camnar Camnarian 
			Camnaril Gelehorn Galindil Ardor Ardorian Artorian Thelrion Adrahel Denarion Calindal Serondor Serondar Serondal Gelmonias Thirendil Thirendir Erlanil Eranil Triandil 
			Evindal Oloris Darandil Alvarion Andrellion
		}
		female_names = {
			# Galixis Iztralis Naven Erelaz Vilnara Celdara Thaliazel Zeluza Imarial Vedaris Vedarszel Varila Varilez Ganuris Varinna Aratha Arathen Amaris 
			# Alaris Ladrindal Leslindal Lithiel Elizna Dazirna Meira Navia Ianen Iaria Narwen Iztrala Varana Galinea Uria Eralia Nestara Vinda Vacera Erana Ulira Andrela
			# Alvara Arda Ivrana Arantha Kalindis Gelmara Zerona Taris Alaris Keladis Elethis Adral Varia Vinara Kalis Jexis
			Galindel Istralania Narawen Erelassa Filinara Celadora Thaliandel Selussa Imariel Vehari Veharia Varila Varilia Panoril Liandiel Varinna Ariathra Ariathen Amarien 
			Alarawel Ladrindel Leslindel Lithiel Elissa Dasirna Meira Novia Ianren Ioria Narwen Istrala Varana Galinea Uria Eralia Nestara Vinda Vacera Erana Olora Andrellia
			Alvara Arda Ivrana Arantha Calindiel Gelmona Serona Ultariel Alaria Celada Elethiel Adral Variel Finoriel Calasiel
		}

		dynasty_of_location_prefix = "dynnp_szel"
		
		patronym_suffix_male = "dynnpat_suf_zuir"
		patronym_suffix_female = "dynnpat_suf_zuir"
		always_use_patronym = yes

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			10 = moon_elvish
			20 = sun_elvish
		}
	}

	wood_elvish = {
		
		color = { 135 245 245 }
		
		cadet_dynasty_names = {
			{ "dynnp_sil" }
			{ "dynnp_sil" }
		}

		dynasty_names = {
			{ "dynnp_sil" }
		}

		male_names = {
			Adrahel Alendir Arahir Aranaris Arfil Barahor Bolrdil Caenlin Camnar Celadil Celmon Cinrthir Daenmir Eledas Elorgaer Erasnir Erelas Erendil Faevir Faldnas 
			Falfin Filliath Galyndor Galynel Gwidulain Imheros Iras Ivrandir Khadoris Khaltec Khoras Meldaer Minelir Narkan Oberon Oloris Paerian Pannreth Panreth 
			Scoraen Tavdan Thalanor Thelrion Thirendil Thocris Trakkath Urion Vaergan Vallorn Varymel Vindal Vucras Vusciran Vyriath Zoriver
		}
		female_names = {
			Aerie Aeriene Alara Amarien Arabeth Arerein Ariathra Bricena Celadora Degindra Dredwaen Elaria Elethen Farathia Farwen Imariel Ingeneth Irann Irvah Isehris 
			Ivrandyel Larelaen Larradhel Liandel Liedir Maberin Maregil Metgael Mialee Mithriel Narawen Nathaleera Nathruviel Nilauin Obera Olfyra Orilenna Pinrynna Selsusa Titariel Trylenna 
			Urariel Varinna Vehari Velacryn Vyhiel
		}

		dynasty_of_location_prefix = "dynnp_sil"

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			20 = moon_elvish
			10 = sun_elvish
		}
	}
}