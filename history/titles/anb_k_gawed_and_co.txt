k_gawed = {
	986.7.6 = {
		holder = 51 #Alenn Gawe
	}
}

k_eaglecrest = {
	1010.4.2 = {
		holder = 56 #Warde Eaglecrest
	}
}

d_beronmoor = {
	990.4.6 = {
		holder = 57 #Garther Beron
	}
}

d_moorhills = {
	1008.7.4 = {
		holder = 58 #Deris Fouler
	}
}

d_westmoor = {
	1003.3.2 = {
		holder = 59 #Marlen Cottersea
	}
}

k_arbaran = {
	1015.9.8 = {
		holder = 60 #Ianren the Rider
	}
}

d_arbaran = {
	1015.9.8 = {
		holder = 60 #Ianren the Rider
	}
}

d_cestir = {
	1015.9.8 = {
		holder = 60 #Ianren the Rider
	}
}

d_golden_plains = {
	1015.9.8 = {
		holder = 60 #Ianren the Rider
	}
}

d_derwing = {
	1015.9.8 = {
		holder = 60 #Ianren the Rider
	}
}

d_teagansfield = {
	1015.9.8 = {
		holder = 60 #Ianren the Rider
	}
}